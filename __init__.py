import os
import re
import time
import glob
import subprocess, json
from kabaret import flow
from kabaret.flow.object import _Manager
from kabaret.flow_contextual_dict import get_contextual_dict
from libreflow.utils.flow.context_values import keywords_from_format
from libreflow.utils.os import remove_folder_content
from libreflow.baseflow import KitsuConfig
from libreflow.baseflow.film import Film
from libreflow.baseflow.file import GenericRunAction, TrackedFile, list_digits
from libreflow.baseflow.runners import FILE_EXTENSION_ICONS


class UploadSettings(flow.Object):
    MANAGER_TYPE = _Manager

    task_statutes = flow.Param(dict)
    audio_root_folder = flow.Param()
    audio_glob_pattern = flow.Param('**/*/{sequence}_{shot}*.mp3')


class RevisionNameChoiceValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _file = flow.Parent(2)

    def __init__(self, parent, name):
        super(RevisionNameChoiceValue, self).__init__(parent, name)
        self._revision_names = None

    def choices(self):
        if self._revision_names is None:
            self._revision_names = self._file.get_revision_names(sync_status='Available', published_only=True)
        
        return self._revision_names
    
    def revert_to_default(self):
        names = self.choices()
        if names:
            self.set(names[-1])
    
    def touch(self):
        self._revision_names = None
        super(RevisionNameChoiceValue, self).touch()


class ExportFormatChoiceValue(flow.values.ChoiceValue):

    CHOICES = ('png', 'psd')


class ExportKrita(GenericRunAction):
    MANAGER_TYPE = _Manager
    ICON = ('icons.gui', 'picture')

    revision = flow.SessionParam(None, RevisionNameChoiceValue)
    export_format = flow.Param('png', ExportFormatChoiceValue).ui(label='Format', choice_icons=FILE_EXTENSION_ICONS)

    _file = flow.Parent()
    _files = flow.Parent(2)
    _task = flow.Parent(3)

    def runner_name_and_tags(self):
        return 'Krita', []
    
    def get_version(self, button):
        return None
    
    def get_run_label(self):
        return 'Krita - Export image'
    
    def allow_context(self, context):
        return (
            context
            and self._file.format.get() == 'kra'
            and self.revision.choices()
        )
    
    def needs_dialog(self):
        self.message.set('<h2>Export image</h2>')
        self.revision.touch()
        self.revision.revert_to_default()
        return True
    
    def get_buttons(self):
        return ['Export', 'Cancel']
    
    def extra_argv(self):
        argv = super(ExportKrita, self).extra_argv()
        
        basename = self._file.name()[:-(len(self._file.format.get()) + 1)]
        scene_path = self._file.get_revision(self.revision.get()).get_path().replace('\\', '/')
        export_path = self._ensure_revision(f'{basename}_export.{self.export_format.get()}', self.revision.get())
        argv += [
            f'{scene_path}',
            '--export',
            '--export-filename',
            f'{export_path}',
        ]
        
        return argv
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        return super(ExportKrita, self).run(button)
    
    def _ensure_revision(self, file_name, revision_name):
        '''
        Returns the path of the revision `revision_name` in
        the `file_name` file history.
        It creates the revision in the project and in the current
        site's file system if it does not exist yet.
        '''
        name, ext = file_name.split('.')
        file_mapped_name = file_name.replace('.', '_')

        default_files = self.root().project().get_task_manager().get_task_files(self._task.name())
        default_file = default_files.get(file_mapped_name)
        
        if not self._files.has_mapped_name(file_mapped_name):
            _file = self._files.add_file(name, ext, tracked=True, default_path_format=(default_file[1] if default_file else None))
        else:
            _file = self._files[file_mapped_name]
        
        _file.file_type.set('Outputs')
        source_revision = self._file.get_revision(self.revision.get())
        revision = _file.get_revision(revision_name)
        
        if revision is None:
            revision = _file.add_revision(revision_name)
        
        revision.comment.set(source_revision.comment.get())
        path = revision.get_path().replace('\\', '/')
        os.makedirs(os.path.dirname(path), exist_ok=True)

        return path


class ImageExportFormat(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'

    def choices(self):
        return [
            'jpg', 'png', 'tif'
        ]


class KritaRenderImageSequence(GenericRunAction):

    ICON = ('icons.libreflow', 'krita')

    revision = flow.SessionParam(None, RevisionNameChoiceValue)

    with flow.group('Advanced settings'):
        frame_prefix = flow.SessionParam(None)
        export_format = flow.SessionParam('png', ImageExportFormat)

    _file = flow.Parent()
    _files = flow.Parent(2)
    _task = flow.Parent(3)

    def runner_name_and_tags(self):
        return 'Krita', []
    
    def get_version(self, button):
        return None
    
    def get_run_label(self):
        return 'Render image sequence'
    
    def allow_context(self, context):
        return False
    
    def needs_dialog(self):
        self.message.set('<h2>Render image sequence</h2>')
        self.revision.touch()
        self.revision.revert_to_default()
        return True
    
    def get_buttons(self):
        return ['Render', 'Cancel']
    
    def extra_argv(self):
        argv = super(KritaRenderImageSequence, self).extra_argv()
        
        basename = self._file.name()[:-(len(self._file.format.get()) + 1)]
        scene_path = self._file.get_revision(self.revision.get()).get_path().replace('\\', '/')
        export_path = self._ensure_render_folder(f'{basename}_render', self.revision.get())
        prefix = self.frame_prefix.get()

        if prefix is None:
            settings = get_contextual_dict(self._file, 'settings')
            sequence_index = list_digits(settings.get('sequence', '0'))[0]
            shot_index = list_digits(settings.get('shot', '0'))[0]
            prefix = f'LPP_CMP_{sequence_index:03}_{shot_index:03}_'
        
        argv += [
            f'{scene_path}',
            '--export-sequence',
            '--export-filename',
            f'{export_path}/{prefix}.{self.export_format.get()}',
        ]
        
        return argv
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        return super(KritaRenderImageSequence, self).run(button)
    
    def _ensure_render_folder(self, folder_name, revision_name):
        '''
        Returns the path of the revision `revision_name` in
        the `folder_name` folder history.
        It creates the revision in the project and in the current
        site's file system if it does not exist yet.
        '''
        default_files = self.root().project().get_task_manager().get_task_files(self._task.name())
        default_file = default_files.get(folder_name)
        
        if not self._files.has_mapped_name(folder_name):
            folder = self._files.add_folder(folder_name, tracked=True, default_path_format=(default_file[1] if default_file else None))
        else:
            folder = self._files[folder_name]
        
        folder.file_type.set('Outputs')
        
        source_revision = self._file.get_revision(self.revision.get())
        revision = folder.get_revision(revision_name)
        
        if revision is None:
            revision = folder.add_revision(revision_name)
        
        revision.comment.set(source_revision.comment.get())
        path = revision.get_path().replace('\\', '/')

        if os.path.isdir(path):
            remove_folder_content(path)
        else:
            os.makedirs(path)

        return path


class KritaRenderPlayblast(flow.Action):

    MANAGER_TYPE = _Manager
    ICON = ('icons.libreflow', 'krita')

    revision = flow.SessionParam(None, RevisionNameChoiceValue)
    
    with flow.group('Advanced settings'):
        output_suffix = flow.SessionParam('').ui(hidden=True)

    _file = flow.Parent()

    def allow_context(self, context):
        return (
            context
            and self._file.format.get() == 'kra'
            and self.revision.choices()
        )
    
    def needs_dialog(self):
        self.message.set('<h2>Render playblast</h2>')
        self.revision.touch()
        self.revision.revert_to_default()
        return True
    
    def get_buttons(self):
        return ['Render', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        revision_name = self.revision.get()
        
        # Render image sequence
        ret = self._render_image_sequence(revision_name)
        render_runner = self.root().session().cmds.SubprocessManager.get_runner_info(
            ret['runner_id']
        )
        # Configure image sequence marking
        folder_name = self._file.name()[:-len(self._file.format.get())]
        folder_name += 'render'
        self._mark_image_sequence(
            folder_name,
            revision_name,
            self.output_suffix.get(),
            render_runner['pid']
        )
    
    def _render_image_sequence(self, revision_name):
        render_action = self._file.render_image_sequence_krita
        render_action.revision.set(revision_name)
        ret = render_action.run('Render')

        return ret
    
    def _mark_image_sequence(self, folder_name, revision_name, output_suffix, render_pid):
        mark_sequence_wait = self._file.mark_image_sequence_wait
        mark_sequence_wait.folder_name.set(folder_name)
        mark_sequence_wait.revision_name.set(revision_name)
        mark_sequence_wait.output_suffix.set(output_suffix)
        mark_sequence_wait.wait_pid(render_pid)
        mark_sequence_wait.run(None)


class KitsuTaskStatusValue(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _task = flow.Parent(4)
    
    def choices(self):
        settings = self.root().project().kitsu_config().upload_settings
        return settings.task_statutes.get().get(self._task.name(), [])
    
    def revert_to_default(self):
        statutes = self.choices()
        if statutes:
            self.set(statutes[0])


class UploadPNGToKitsu(flow.Action):
    '''
    Export a PNG file from a Krita scene and upload it to Kitsu.
    '''

    MANAGER_TYPE = _Manager
    ICON = ('icons.libreflow', 'kitsu')

    revision = flow.SessionParam(None, RevisionNameChoiceValue)
    status = flow.SessionParam(None, KitsuTaskStatusValue)
    comment = flow.SessionParam('')

    _file = flow.Parent()
    _files = flow.Parent(2)

    def allow_context(self, context):
        return (
            context
            and self._file.format.get() == 'kra'
            and self.revision.choices()
        )
    
    def needs_dialog(self):
        self.message.set('<h2>Upload PNG to Kitsu</h2>')
        self.revision.touch()
        self.revision.revert_to_default()
        self.status.revert_to_default()
        return True

    def get_buttons(self):
        return ['Upload', 'Cancel']

    def run(self, button):
        if button == 'Cancel':
            return

        export_action = self._file.export_krita
        export_action.revision.set(self.revision.get())
        export_action.export_format.set('png')
        result = export_action.run('Render')
        runner_info = self.root().session().cmds.SubprocessManager.get_runner_info(result['runner_id'])
        print('Export PNG :: Waiting for export to finish...')

        while runner_info['is_running']:
            time.sleep(1)
            runner_info = self.root().session().cmds.SubprocessManager.get_runner_info(result['runner_id'])
        
        print('Export PNG :: Upload export to Kitsu')
        
        kitsu_bindings = self.root().project().kitsu_bindings()
        kitsu_api = self.root().project().kitsu_api()

        basename = self._file.name()[:-(len(self._file.format.get()) + 1)]
        task_type = kitsu_bindings.task_type_files.get().get(f'{basename}_export.png')
        export_path = None

        if task_type is not None and self._files.has_mapped_name(f'{basename}_export_png'):
            export_file = self._files[f'{basename}_export_png'].get_revision(self.revision.get())
            export_path = export_file.get_path()

            if os.path.exists(export_path):
                settings = get_contextual_dict(self, 'settings')

                if 'asset' in settings:
                    kitsu_api.upload_asset_preview(
                        settings['asset'],
                        task_type,
                        self.status.get(),
                        export_path,
                        self.comment.get())
                elif 'shot' in settings and 'sequence' in settings:
                    kitsu_api.upload_shot_preview(
                        settings['sequence'],
                        settings['shot'],
                        task_type,
                        self.status.get(),
                        export_path,
                        self.comment.get())
                else:
                    # undefined entity
                    print('Export PNG :: WARNING: target entity not found')


class CheckShotFramesSettings(flow.Object):

    task_name_for_hide = flow.Param().ui(label="Task name for done shots")


class CheckShotFrames(flow.Action):
    '''
    Check frames count in all shots by comparing with Kitsu
    '''
    MANAGER_TYPE = _Manager
    ICON = ('icons.libreflow', 'kitsu')

    settings = flow.Child(CheckShotFramesSettings)

    _film = flow.Parent()

    def kitsu_api(self):
        return self.root().project().kitsu_api()

    def get_exec_path(self):
        site_env = self.root().project().get_current_site().site_environment
        
        if site_env.has_mapped_name('FFPROBE_EXEC_PATH'):
            return site_env['FFPROBE_EXEC_PATH'].value.get()
        else:
            return None

    def get_shots(self):
        shot_list = []
        
        for seq in self._film.sequences.mapped_items():
            for shot in seq.shots.mapped_items():
                shot_list.append([seq.oid(), shot.oid()])
        
        return shot_list

    def get_playblasts(self, shot_oid):
        playblast_list = []
        
        shot = self.root().get_object(shot_oid)

        for task in shot.tasks.mapped_items():
            for f in task.files.mapped_items():
                if f.format.get() == 'mov':
                    playblast_list.append([task.oid(), f.oid()])

        return playblast_list

    def _fill_ui(self, ui):
        ui['custom_page'] = 'libreflow.extensions.file_manager.kitsu.ui.check_frames.CheckShotFramesWidget'


class AudioRootFolder(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _action = flow.Parent()

    def choices(self):
        root_dir = self.root().project().kitsu_config().upload_settings.audio_root_folder.get()

        try:
            dirs = os.listdir(root_dir)
        except FileNotFoundError:
            dirs = []
        else:
            dirs = [
                d for d in dirs
                if self._action.audio_tracks(d)
            ]
        
        return dirs
    
    def revert_to_default(self):
        names = self.choices()
        if names:
            self.set(names[-1])
        else:
            self.set(None)


class AudioTrackFile(flow.values.SessionValue):

    DEFAULT_EDITOR = 'choice'
    STRICT_CHOICES = False

    _action = flow.Parent()

    def choices(self):
        return self._action.audio_tracks(self._action.folder.get())
    
    def revert_to_default(self):
        names = self.choices()
        if names:
            self.set(names[-1])
        else:
            self.set(None)


class AddAudioTrack(flow.Action):
    MANAGER_TYPE = _Manager

    revision = flow.SessionParam(value_type=RevisionNameChoiceValue)

    with flow.group('Manual selection'):
        folder = flow.SessionParam(value_type=AudioRootFolder).watched()
        audio_track = flow.SessionParam(value_type=AudioTrackFile)
    
    _audio_pattern = flow.Computed(cached=True)

    _file       = flow.Parent()
    _shot       = flow.Parent(5)
    _sequence   = flow.Parent(7)

    def needs_dialog(self):
        title = '<h2>Add audio track</h2>'
        self._audio_pattern.touch()
        self.revision.revert_to_default()
        self.folder.revert_to_default()
        r = self._file.get_revision(self.revision.get())

        if not os.path.isfile(r.get_path()):
            self.message.set(title + '<font color=#d5000d>The selected revision doesn\'t exist.</font>')
        elif self.folder.get() is None or self.audio_track.get() is None:
            self.message.set(title + '<font color=#d5000d>No audio track found !</font>')
        elif len(self.audio_track.choices()) > 1:
            self.message.set(title + 'Multiple audio tracks found: you can select it manually.')
        else:
            self.message.set(title + 'The selected revision will be overwritten.')
        
        return True
    
    def allow_context(self, context):
        return context and self._file.get_head_revision() is not None

    def get_buttons(self):
        if self.folder.get() is None or self.audio_track.get() is None:
            return ['Cancel']
        
        src_path = self._file.get_revision(self.revision.get()).get_path()

        if not os.path.isfile(src_path):
            return ['Cancel']
        
        return ['Confirm', 'Cancel']
    
    def run(self, button):
        if button == 'Cancel':
            return
        
        src_path = self._file.get_revision(self.revision.get()).get_path()
        dst_path = os.path.join(os.path.dirname(src_path), 'tmp__' + os.path.basename(src_path))
        settings = self.root().project().kitsu_config().upload_settings
        
        subprocess.run([
            'ffmpeg',
            '-i', src_path,
            '-i', self.audio_track.get(),
            '-c:v', 'copy', '-c:a', 'aac', '-b:a', '160k',
            dst_path
        ])

        os.remove(src_path)
        os.rename(dst_path, src_path)
    
    def compute_child_value(self, child_value):
        if child_value is self._audio_pattern:
            settings = self.root().project().kitsu_config().upload_settings
            pattern = settings.audio_glob_pattern.get()
            keywords = keywords_from_format(pattern)

            if keywords:
                context_settings = get_contextual_dict(self._file, 'settings')

                if any([kw not in context_settings for kw in keywords]): # Pattern with other keywords than sequence and shot names is invalid
                    pattern = None
                else:
                    keywords = {
                        key: context_settings[key]
                        for key in keywords
                    }
                    pattern = pattern.format_map(keywords)
            
            self._audio_pattern.set(pattern)
    
    def child_value_changed(self, child_value):
        if child_value is self.folder:
            self.audio_track.revert_to_default()
    
    def audio_tracks(self, dir_name):
        settings = self.root().project().kitsu_config().upload_settings
        root_dir = settings.audio_root_folder.get()
        audio_pattern = self._audio_pattern.get()

        if dir_name is None or root_dir is None or audio_pattern is None:
            return []
        
        files = glob.glob(os.path.join(root_dir, dir_name, audio_pattern), recursive=True)

        return files


def render_playblast_krita(parent):
    if isinstance(parent, TrackedFile) and parent.name() == 'animation_kra':
        render_img_sequence = flow.Child(KritaRenderImageSequence)
        render_img_sequence.name = 'render_image_sequence_krita'
        render_img_sequence.index = None
        render_playblast = flow.Child(KritaRenderPlayblast).ui(label='Render playblast')
        render_playblast.name = 'render_playblast_krita'
        render_playblast.index = 30
        return [render_img_sequence, render_playblast]


def export_png_krita(parent):
    if isinstance(parent, TrackedFile) and parent.name() == 'layout_kra':
        r = flow.Child(ExportKrita).ui(label='Export image')
        r.name = 'export_krita'
        r.index = 31
        return r


def upload_settings(parent):
    if isinstance(parent, KitsuConfig):
        r = flow.Child(UploadSettings)
        r.name = 'upload_settings'
        r.index = None
        return r


def upload_png_to_kitsu(parent):
    if isinstance(parent, TrackedFile) and parent.format.get() == 'kra':
        r = flow.Child(UploadPNGToKitsu)
        r.name = 'upload_png_to_kitsu'
        r.index = 32
        return r


def add_audio_track(parent):
    if isinstance(parent, TrackedFile) and parent.format.get() == 'mov':
        r = flow.Child(AddAudioTrack)
        r.name = 'add_audio_track'
        r.index = None
        return r


def check_shot_frames(parent):
    if isinstance(parent, Film):
        r = flow.Child(CheckShotFrames).ui(dialog_size=(800,600))
        r.name = 'check_shot_frames'
        r.index = None
        return r


def install_extensions(session):
    return {
        'krita_rendering': [
            render_playblast_krita,
            export_png_krita,
        ],
        'upload': [
            upload_settings,
            upload_png_to_kitsu,
            add_audio_track,
        ],
        'misc': [
            check_shot_frames
        ]
    }


# def get_handlers():
#     return [
#         ("^/[^/]+/admin/kitsu$", "upload_settings", classqualname(UploadSettings), {}),
#         (".*/tasks/[^/]+/files/[^/]+_kra$", "upload_png_to_kitsu", classqualname(UploadPNGToKitsu), {'label': 'Upload to Kitsu'}),
#         (".*/tasks/[^/]+/files/[^/]+_mov$", "add_audio_track", classqualname(AddAudioTrack), {'label': 'Add audio track'}),
#         ("^/[^/]+/films/[^/]+(_test)?$", "check_shot_frames", classqualname(CheckShotFrames), {'label': 'Check Shot Frames', 'custom_page': 'libreflow.extensions.file_manager.kitsu.ui.check_frames.CheckShotFramesWidget'})
#     ]