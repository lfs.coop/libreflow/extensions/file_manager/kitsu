import subprocess, json
import pprint
from kabaret.app.ui.gui.widgets.flow.flow_view import (
    CustomPageWidget,
    QtWidgets,
    QtCore,
    QtGui,
)
from kabaret.app import resources

from libreflow.baseflow.runners import FILE_EXTENSION_ICONS

from libreflow.resources.icons import gui as _


class RunnerSignals(QtCore.QObject):
    
    # Signals for QRunnable must be outside the class.

    started = QtCore.Signal(object)
    progress = QtCore.Signal(object, str)


class CheckRunner(QtCore.QRunnable):
    
    # Check frames count of playblast file with ffprobe process

    def __init__(self, item):
        super(CheckRunner, self).__init__()
        self.item = item
        self.custom_widget = item.custom_widget

        self.signals = RunnerSignals()

    def run(self):
        self.signals.started.emit(self.item)

        check_frames = subprocess.check_output(
            f'{self.custom_widget.exec_path} -v quiet -show_streams -select_streams v:0 -of json "{self.item.rev.get_path()}"',
            shell=True).decode()

        fields = json.loads(check_frames)['streams'][0]
        frames = fields['nb_frames']

        self.signals.progress.emit(self.item, frames)


class FileItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, item, task_oid, file_oid):
        super(FileItem, self).__init__(item)
        self.tree = item.tree
        self.custom_widget = item.custom_widget
        self.shot = item
        self.task_oid = task_oid
        self.item_oid = file_oid

        self.refresh()

    def refresh(self):
        icon = FILE_EXTENSION_ICONS.get(
            self.custom_widget.session.cmds.Flow.get_value(self.item_oid+'/format'), ('icons.gui', 'text-file-1')
        )
        self.rev = self.custom_widget.session.cmds.Flow.call(self.item_oid, "get_head_revision", [], {})

        self.setIcon(0, self.get_icon(icon))
        self.setText(0, '{task} {file} {rev}'.format(
            task=self.custom_widget.session.cmds.Flow.call(self.task_oid, "name", [], {}),
            file=self.custom_widget.session.cmds.Flow.get_value(self.item_oid+'/display_name'),
            rev=self.rev.name()
        ))

        shot_data = self.custom_widget.kitsu_api().get_shot_data(self.shot.shot_name, self.shot.sequence_name)
        self.setText(1, str(shot_data['nb_frames']))
        
        self.check_thread = CheckRunner(self)
        self.check_thread.signals.started.connect(self.custom_widget.update_icon)
        self.check_thread.signals.progress.connect(self.custom_widget.set_file_frames)

    @staticmethod
    def get_icon(icon_ref):
        return QtGui.QIcon(resources.get_icon(icon_ref))


class ShotItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, tree, seq_oid, shot_oid):
        super(ShotItem, self).__init__(tree)
        self.tree = tree
        self.custom_widget = tree.custom_widget
        self.seq_oid = seq_oid
        self.item_oid = shot_oid

        self.sequence_name = self.custom_widget.session.cmds.Flow.call(self.seq_oid, "name", [], {})
        self.shot_name = self.custom_widget.session.cmds.Flow.call(self.item_oid, "name", [], {})

        self.refresh()
        
    def refresh(self):
        self.setExpanded(True)
        self.setIcon(0, self.get_icon(('icons.flow', 'sequence')))
        self.setText(0, '{sequence} {shot}'.format(
            sequence=self.sequence_name,
            shot=self.shot_name
        ))

        file_oids = self.custom_widget.get_playblasts(self.item_oid)

        for oid in file_oids:
            FileItem(self, oid[0], oid[1])
        
        for i in range(self.treeWidget().header().count()):
            brush = QtGui.QBrush(QtGui.QColor(70, 70, 70))
            self.setBackground(i, brush)

    @staticmethod
    def get_icon(icon_ref):
        return QtGui.QIcon(resources.get_icon(icon_ref))


class PlayblastList(QtWidgets.QTreeWidget):

    def __init__(self, custom_widget):
        super(PlayblastList, self).__init__()
        self.custom_widget = custom_widget

        self.setHeaderLabels(self.get_columns())
        self.setSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        
        self.refresh()

        self.header().resizeSections(QtWidgets.QHeaderView.ResizeToContents)
        self.setColumnWidth(0, self.columnWidth(0)+30)

        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._on_context_menu)

    def get_columns(self):
        return ('Playblast', 'Kitsu Frames', 'File Frames', 'Status')

    def refresh(self, force_update=False):
        self.blockSignals(True)
        self.clear()

        shot_oids = self.custom_widget.get_shots()

        task_name = self.custom_widget.session.cmds.Flow.get_value(
            self.custom_widget.oid+'/settings/task_name_for_hide'
        )

        for oid in shot_oids:
            if task_name:
                if self.custom_widget.kitsu_api().get_shot_task_status_name(
                    self.custom_widget.session.cmds.Flow.call(oid[0], "name", [], {}),
                    self.custom_widget.session.cmds.Flow.call(oid[1], "name", [], {}), 
                    task_name
                ) == "Done":
                    continue

            ShotItem(self, oid[0], oid[1])

        self.blockSignals(False)

    @staticmethod
    def get_icon(icon_ref):
        return QtGui.QIcon(resources.get_icon(icon_ref))

    def _on_context_menu(self, event):
        item = self.itemAt(event)
        column = self.currentColumn()

        if item is None:
            return

        context_menu = QtWidgets.QMenu(self)

        if isinstance(item, FileItem):
            open_file = context_menu.addAction(self.get_icon(('icons.gui', 'open-folder')), 'Open')
            open_file.triggered.connect(
                lambda checked=False, x=item: self._on_open_file_action_clicked(x)
            )

        if context_menu.isEmpty() != True:
            context_menu.exec_(self.mapToGlobal(event))

    def _on_open_file_action_clicked(self, item):
        self.custom_widget.page.show_action_dialog(self.custom_widget.session.cmds.Flow.call(
            item.rev.oid(), 'activate_oid', [], {}
        ))


class CheckShotFramesWidget(CustomPageWidget):

    def build(self):
        # To multithread ffprobe
        self.__pool = QtCore.QThreadPool()
        self.__pool.setMaxThreadCount(self.__pool.globalInstance().maxThreadCount())

        self.exec_path = self.get_exec_path()
        glo = QtWidgets.QGridLayout()

        if self.exec_path is not None:
            self.playblast_list = PlayblastList(self)
            self.shots_count = QtWidgets.QLabel(str(self.get_playblasts_count())+' playblasts')
            self.button_settings = QtWidgets.QPushButton('Settings')
            self.button_settings.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
            self.button_settings.clicked.connect(self._on_button_settings_clicked)
            self.button_settings.setAutoDefault(False)

            glo.addWidget(self.shots_count, 0, 0)
            glo.addWidget(self.playblast_list, 1, 0)
            glo.addWidget(self.button_settings, 2, 0)
        else:
            warning_widget = QtWidgets.QWidget()
            warning_layout = QtWidgets.QVBoxLayout()

            icon = QtGui.QIcon(resources.get_icon(('icons.gui', 'exclamation-sign')))
            pixmap = icon.pixmap(QtCore.QSize(128, 128))
            icon_lbl = QtWidgets.QLabel('')
            icon_lbl.setPixmap(pixmap)

            text = QtWidgets.QLabel('FFProbe path is not configured in site environment')

            warning_layout.addWidget(icon_lbl, 0, QtCore.Qt.AlignCenter)
            warning_layout.addWidget(text, 1, QtCore.Qt.AlignCenter)
            warning_widget.setLayout(warning_layout)
            
            glo.addWidget(warning_widget, 0, 0, QtCore.Qt.AlignCenter)
        
        self.setLayout(glo)

        # When build is finished
        # Start threads
        for i in range(self.playblast_list.topLevelItemCount()):
            shot = self.playblast_list.topLevelItem(i)
            if shot:
                for m in range(shot.childCount()):
                    movie = shot.child(m)
                    if movie:
                        self.__pool.start(movie.check_thread)

    def sizeHint(self):
        return QtCore.QSize(600, 500)

    def get_exec_path(self):
        return self.session.cmds.Flow.call(
            self.oid, 'get_exec_path', [], {}
        )

    def kitsu_api(self):
        return self.session.cmds.Flow.call(
            self.oid, 'kitsu_api', [], {}
        )
    
    def get_shots(self):
        return self.session.cmds.Flow.call(
            self.oid, 'get_shots', [], {}
        )

    def get_playblasts(self, shot_oid):
        return self.session.cmds.Flow.call(
            self.oid, 'get_playblasts', [shot_oid], {}
        )

    def get_playblasts_count(self):
        count = 0
        for i in range(self.playblast_list.topLevelItemCount()):
            count += self.playblast_list.topLevelItem(i).childCount()
        return count

    # Started signal for CheckRunner
    def update_icon(self, item):
        item.setIcon(2, item.get_icon(('icons.libreflow', 'waiting')))
    
    # Progress signal for CheckRunner
    def set_file_frames(self, item, count):
        item.setIcon(2, QtGui.QIcon())
        item.setText(2, count)
        item.setIcon(3, item.get_icon(
            ('icons.libreflow', 'unavailable') if int(count) != int(item.text(1)) else ('icons.libreflow', 'available')
        ))
        if int(count) != int(item.text(1)):
            item.setText(3, str(int(count) - int(item.text(1))))

    def _on_button_settings_clicked(self):
        self.page.goto(self.oid + '/settings')
